# Repo for [rfw](https://rapefreeworld.com/) site.

# Development
1. clone repo
2. run "npm install"
3. run "npm run start"

# Test production 
1. build image with "docker build -t rfw:v1 ."
2. start image with "docker run -d -p 80:80 rfw:v1"

# Deployment
1. create tar file with "tar -czvf rfw-v1.tar.gz ."
2. upload file to caprover 